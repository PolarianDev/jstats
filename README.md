# Jstats - A Java IRC Statistics Bot
## About:
Jstats is a IRC bot which tracks interesting statistics, without being invasive. It tracks:
- Number of messages which are sent on the server its connected to (in the channels it has joined)
- Number of messages which are sent in each specific channel it has joined
- Number of messages each user has sent

As privacy is important, the bot does not store the content of each message, nor does it use the statistics for any other use other than providing interesting numbers, and can also be used to see who speaks the most in each specific channel.

Jstats is self hosted, this therefore means the host of the bot stores all the data, this means that no external party has access to the data stored, further protecting your privacy.

## Setup:
A full setup guide will be added upon inital release, currently the project is WIP and therefore the setup can not be published at this moment in time. Furthermore, the master branch is not guaranteed to be functional until the initial release.

## Bugs, issues or desired features?
Please submit an issue, this includes the features which you would like to be added. I am happy to attempt to implement the ideas, however if they are too privacy invasive, I may refuse (for example asking for it to track the content of the messages people send).