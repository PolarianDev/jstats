package dev.polarian.jstats;

import dev.polarian.ircj.IrcClient;
import dev.polarian.jstats.database.H2Connection;

import dev.polarian.jstats.listeners.*;

import java.io.IOException;

public class Bot {
    public static BotConfig config;
    public static H2Connection db;
    public static IrcClient client;
    public static String connectedServer;

    public static void main(String[] args) throws IOException {
        config = ConfigHandler.loadConf();
        assert config != null;
        client = new IrcClient(config);

        // Add listeners
        client.addChannelJoinEventListener(new OnChannelJoinEvent());
        client.addWelcomeEventListener(new OnWelcomeEvent());
        client.addPrivMessageEventListener(new OnPrivMessageEvent());

        client.connect();
    }
}
