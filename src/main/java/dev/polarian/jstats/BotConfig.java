package dev.polarian.jstats;

import dev.polarian.ircj.objects.Config;

public class BotConfig extends Config {
    private String prefix;

    public String getPrefix() {
        return this.prefix;
    }

    public BotConfig setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }
}
