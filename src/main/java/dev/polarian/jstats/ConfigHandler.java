package dev.polarian.jstats;

import com.moandjiezana.toml.Toml;
import dev.polarian.jstats.tables.CommandTable;
import dev.polarian.jstats.tables.ConnectionTable;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ConfigHandler {

    private static void genConf() {
        try {
            InputStream defConf = Bot.class.getResourceAsStream("/config.toml");
            assert defConf != null;
            Files.copy(defConf, Path.of("config.toml"));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to generate config, exiting...");
            System.exit(1);
        }
        System.out.println("Config has been generated successfully! exiting...");
        System.exit(0);
    }

    public static BotConfig loadConf() {
        try {
            Toml toml = new Toml().read(new FileInputStream("config.toml"));
            BotConfig config = new BotConfig();

            // load tables
            ConnectionTable connection = toml.getTable("Connection").to(ConnectionTable.class);
            CommandTable command = toml.getTable("Command").to(CommandTable.class);

            config.setUsername(connection.username);
            config.setChannels(List.of(connection.channels));
            config.setHostname(connection.host);
            config.setNickname(connection.username);
            config.setPort(connection.port);
            config.setTimeout(connection.timeout * 1000);
            config.setTls(connection.tls);
            config.setPrefix(command.prefix);
            return config;
        } catch (IOException e) {
            System.out.println("Failed to find config.toml, attempting to generate config...");
            genConf();
            return null;
        }
    }
}
