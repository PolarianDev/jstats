package dev.polarian.jstats.database;

import java.sql.*;
import java.util.Optional;

import static dev.polarian.jstats.Bot.connectedServer;

public class H2Connection {
    private Connection connection;

    public H2Connection() {
        try {
            connection = DriverManager.getConnection("jdbc:h2:./data", "admin", "");
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void createTables() {
        Statement statement;
        try {
            statement = connection.createStatement();

            statement.addBatch("CREATE TABLE IF NOT EXISTS servermessagecount(server VARCHAR(255), count INT);");
            statement.addBatch("CREATE TABLE IF NOT EXISTS channelmessagecount(channel VARCHAR(255), count INT);");
            statement.addBatch("CREATE TABLE IF NOT EXISTS usermessagecount(user VARCHAR(255), count INT);");

            statement.executeBatch();
            connection.commit();
            statement.close();

            Optional<Boolean> exists = checkExists(Tables.SERVERMESSAGECOUNT, connectedServer);
            if (exists.isEmpty()) {
                System.exit(1);
            } else {
                if (!exists.get()) {
                    createServerRow(connectedServer);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void incrementMessages(String channel, String user) {
        Optional<Boolean> exists = checkExists(Tables.CHANNELMESSAGECOUNT, channel);
        try {
            updateMessageCount(Tables.SERVERMESSAGECOUNT, connectedServer);
            if (exists.isPresent() && exists.get()) {
                updateMessageCount(Tables.CHANNELMESSAGECOUNT, channel);
            } else {
                insertMessageCount(Tables.CHANNELMESSAGECOUNT, channel);
            }
            exists = checkExists(Tables.USERMESSAGECOUNT, user);
            if (exists.isPresent() && exists.get()) {
                updateMessageCount(Tables.USERMESSAGECOUNT, user);
            } else {
                insertMessageCount(Tables.USERMESSAGECOUNT, user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Boolean> checkExists(Tables table, String identifier) {
        PreparedStatement statement = null;
        boolean ret;
        try {
            switch (table) {
                case SERVERMESSAGECOUNT:
                    statement = connection.prepareStatement("SELECT EXISTS(SELECT server FROM servermessagecount WHERE server=?)");
                    break;
                case CHANNELMESSAGECOUNT:
                    statement = connection.prepareStatement("SELECT EXISTS(SELECT channel FROM channelmessagecount WHERE channel=?)");
                    break;
                case USERMESSAGECOUNT:
                    statement = connection.prepareStatement("SELECT EXISTS(SELECT user FROM usermessagecount WHERE user=?)");
                    break;
            }
            statement.setString(1, identifier);
            ResultSet result = statement.executeQuery();
            result.next();
            ret = result.getBoolean(1);
            statement.close();
            return Optional.of(ret);
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public void insertMessageCount(Tables table, String row) throws SQLException {
        PreparedStatement statement = null;
        switch (table) {
            case CHANNELMESSAGECOUNT:
                statement = connection.prepareStatement("INSERT INTO channelmessagecount(channel, count) VALUES(?, 1)");
                break;
            case USERMESSAGECOUNT:
                statement = connection.prepareStatement("INSERT INTO usermessagecount(user, count) VALUES(?, 1)");
                break;
        }
        assert statement != null;
        statement.setString(1, row);
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    public void updateMessageCount(Tables table, String row) throws SQLException {
        PreparedStatement statement = null;
        switch (table) {
            case SERVERMESSAGECOUNT:
                statement = connection.prepareStatement("UPDATE servermessagecount SET count=count+1 WHERE server=?");
                break;
            case CHANNELMESSAGECOUNT:
                statement = connection.prepareStatement("UPDATE channelmessagecount SET count=count+1 WHERE channel=?");
                break;
            case USERMESSAGECOUNT:
                statement = connection.prepareStatement("UPDATE usermessagecount SET count=count+1 WHERE user=?");
                break;
        }
        assert statement != null;
        statement.setString(1, row);
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    public int getMessageCount(Tables table, String row) throws SQLException {
        PreparedStatement statement = null;
        switch (table) {
            case USERMESSAGECOUNT -> statement = connection.prepareStatement("SELECT count FROM usermessagecount WHERE user=?");
            case CHANNELMESSAGECOUNT -> statement = connection.prepareStatement("SELECT count FROM channelmessagecount WHERE channel=?");
            case SERVERMESSAGECOUNT -> statement = connection.prepareStatement("SELECT count FROM servermessagecount WHERE server=?");
        }
        assert statement != null;
        statement.setString(1, row);
        ResultSet result = statement.executeQuery();
        result.next();
        int count = result.getInt(0);
        result.close();
        statement.close();
        return count;
    }

    public void createServerRow(String server) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("INSERT INTO servermessagecount(server, count) VALUES (?, 0)");
        statement.setString(1, server);
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }
}
