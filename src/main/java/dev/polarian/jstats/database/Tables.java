package dev.polarian.jstats.database;

public enum Tables {
    SERVERMESSAGECOUNT,
    CHANNELMESSAGECOUNT,
    USERMESSAGECOUNT,
}
