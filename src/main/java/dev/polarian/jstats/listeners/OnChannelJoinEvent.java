package dev.polarian.jstats.listeners;

import dev.polarian.ircj.events.ChannelJoinEvent;
import dev.polarian.ircj.objects.messages.ChannelJoinMessage;

public class OnChannelJoinEvent implements ChannelJoinEvent {
    @Override
    public void invoke(ChannelJoinMessage message) {
        System.out.println("Joined " + message.getChannel());
    }
}
