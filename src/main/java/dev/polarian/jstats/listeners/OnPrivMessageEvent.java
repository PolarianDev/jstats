package dev.polarian.jstats.listeners;

import dev.polarian.ircj.events.PrivMessageEvent;
import dev.polarian.ircj.objects.messages.PrivMessage;
import dev.polarian.jstats.database.Tables;

import java.io.IOException;
import java.sql.SQLException;

import static dev.polarian.jstats.Bot.*;

public class OnPrivMessageEvent implements PrivMessageEvent {
    @Override
    public void invoke(PrivMessage message) {
        String content = message.getMessage();
        String prefix = config.getPrefix();
        if (content.contains("statbot")) {
            try {
                message.sendMessage(client, message.getChannel(), "Hello there " + message.getNick());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        db.incrementMessages(message.getChannel(), message.getNick());
        if (content.startsWith(prefix)) {
            int i = content.indexOf(" ");
            int prefixLength = prefix.length();
            String command;
            if (i == -1) {
                command = content.substring(prefixLength);
            } else {
                command = content.substring(prefixLength, i).toLowerCase();
            }
            System.out.println(command);
            switch (command) {
                case "hello":
                    try {
                        message.sendMessage(client, message.getChannel(), "Hello there " + message.getNick());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                case "servermsgs":
                    try {
                        int count = db.getMessageCount(Tables.SERVERMESSAGECOUNT, message.getNick());
                        message.sendMessage(client, message.getChannel(), String.format("%s messages have been sent on %s", count, connectedServer));
                    } catch (SQLException | IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
