package dev.polarian.jstats.listeners;

import dev.polarian.ircj.objects.messages.WelcomeMessage;
import dev.polarian.jstats.database.H2Connection;
import dev.polarian.ircj.events.WelcomeEvent;

import static dev.polarian.jstats.Bot.db;
import static dev.polarian.jstats.Bot.connectedServer;

public class OnWelcomeEvent implements WelcomeEvent {
    @Override
    public void invoke(WelcomeMessage message) {
        System.out.println("Successfully connected to server...");
        connectedServer = message.getServerName();
        db = new H2Connection();
    }
}
