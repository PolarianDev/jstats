package dev.polarian.jstats.tables;

public class ConnectionTable {
    public String host, username;
    public int port, timeout;
    public boolean tls;
    public String[] channels;
}
